﻿using Identity.Domain.Dto;
using Identity.Domain.Entities;
using Identity.Infrastructure.Context;
using Identity.Infrastructure.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Shared.Core;
using Shared.Core.Extensions;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Identity.Infrastructure.Services;

/// <summary>
///
/// </summary>
/// <param name="appSettings"></param>
/// <param name="context"></param>
public class UserService( IOptions<AppSettings> appSettings, IIdentityDbContext context ) : IUserService
{
    /// <summary>
    ///
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    public async Task<AuthenticateResponse?> Authenticate(AuthenticateRequest model)
    {
        var user = await context.Users.SingleOrDefaultAsync(x => x.Username == model.Username && x.Password == model.Password);
        return user == null
            ? null
            : new AuthenticateResponse(user, await GenerateJwtToken(user));
    }

    /// <summary>
    ///
    /// </summary>
    /// <returns></returns>
    public async Task<IEnumerable<UserEntity>> GetAll()
    {
        return await context
            .Users
            .Where(x => x.IsActive == true)
            .ToListAsync();
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public async Task<UserEntity?> GetById(Guid id)
    {
        return await context.Users.FirstOrDefaultAsync(x => x.Id == id);
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="userDto"></param>
    /// <returns></returns>
    public async Task<UserDto?> AddAndUpdateUser(UserDto userDto)
    {
        var isSuccess = false;

        if (userDto.Id != Guid.Empty || userDto.Id != default)
        {
            var obj = await context.Users.FirstOrDefaultAsync(c => c.Id == userDto.Id);
            if (obj == null)
                return isSuccess ? userDto : null;

            obj.FirstName = userDto.FirstName;
            obj.LastName = userDto.LastName;
            context.Users.Update(obj);
            isSuccess = await context.SaveChangesAsync() > 0;
        }
        else
        {
            await context.Users.AddAsync(new UserEntity
            {
                Id = Guid.NewGuid(),
                Created = default,
                CreatedBy = default,
                LastModified = null,
                LastModifiedBy = null,
                FirstName = userDto.FirstName,
                LastName = userDto.LastName,
                Username = userDto.Username,
                Password = userDto.Password.Hash(),
                IsActive = true
            });
            isSuccess = await context.SaveChangesAsync() > 0;
        }

        return isSuccess ? userDto : null;
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="user"></param>
    /// <returns></returns>
    private async Task<string> GenerateJwtToken(UserEntity user)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        var token = await Task.Run(() =>
        {
            var key = Encoding.ASCII.GetBytes(appSettings.Value.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", user.Id.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            return tokenHandler.CreateToken(tokenDescriptor);
        });

        return tokenHandler.WriteToken(token);
    }
}