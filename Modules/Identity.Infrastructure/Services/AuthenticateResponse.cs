﻿using Identity.Domain.Entities;

namespace Identity.Infrastructure.Services;

/// <summary>
///
/// </summary>
/// <param name="user"></param>
/// <param name="token"></param>
public class AuthenticateResponse( UserEntity user, string token )
{
    public Guid Id { get; set; } = user.Id;
    public string FirstName { get; set; } = user.FirstName;
    public string LastName { get; set; } = user.LastName;
    public string Username { get; set; } = user.Username;
    public string Token { get; set; } = token;
}