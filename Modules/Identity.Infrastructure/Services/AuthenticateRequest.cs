﻿using Shared.Core.Extensions;

namespace Identity.Infrastructure.Services;

/// <summary>
///
/// </summary>
public class AuthenticateRequest
{
    public string Username { get; set; }
    public string Password { get; set; }
}