﻿using Identity.Domain.Dto;
using Identity.Domain.Entities;

namespace Identity.Infrastructure.Services.Interfaces;

/// <summary>
///
/// </summary>
public interface IUserService
{
    /// <summary>
    ///
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    Task<AuthenticateResponse?> Authenticate(AuthenticateRequest model);

    /// <summary>
    ///
    /// </summary>
    /// <returns></returns>
    Task<IEnumerable<UserEntity>> GetAll();

    /// <summary>
    ///
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    Task<UserEntity?> GetById(Guid id);

    /// <summary>
    ///
    /// </summary>
    /// <param name="userDto"></param>
    /// <returns></returns>
    Task<UserDto?> AddAndUpdateUser(UserDto userDto);
}