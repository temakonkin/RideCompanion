﻿using Identity.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Identity.Infrastructure.Context;

/// <summary>
///
/// </summary>
public interface IIdentityDbContext : IDisposable
{
    DbSet<UserEntity> Users { get; set; }

    Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
}