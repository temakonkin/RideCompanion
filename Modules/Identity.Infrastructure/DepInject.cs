﻿using Identity.Infrastructure.Context;
using Identity.Infrastructure.Services;
using Identity.Infrastructure.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Identity.Infrastructure;

/// <summary>
///
/// </summary>
public static class DepInject
{
    public static void InjectIdentity(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContext<IdentityDbContext>(
            options => options.UseNpgsql(configuration.GetConnectionString("IdentityDbConnection"),
            b => b.MigrationsAssembly("Shared.Migrations")));

        services.AddScoped<IIdentityDbContext, IdentityDbContext>();
        services.AddScoped<IUserService, UserService>();
    }
}