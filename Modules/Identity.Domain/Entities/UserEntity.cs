﻿using Shared.Core.BaseEntities;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Identity.Domain.Entities;

/// <summary>
///
/// </summary>
public class UserEntity : BaseAuditableEntity
{
    /// <summary>
    ///
    /// </summary>
    [MaxLength(50)]
    public required string FirstName { get; set; }

    /// <summary>
    ///
    /// </summary>
    [MaxLength(50)]
    public required string LastName { get; set; }

    /// <summary>
    ///
    /// </summary>
    [MaxLength(50)]
    public required string Username { get; set; } = string.Empty;

    /// <summary>
    ///
    /// </summary>
    [JsonIgnore]
    [MaxLength(50)]
    public required string Password { get; set; }
    public bool IsActive { get; set; }
}