using Shared.Core.BaseDtos;

namespace Identity.Domain.Dto;

/// <summary>
///
/// </summary>
public class UserDto : BaseAuditableDto
{
    /// <summary>
    ///
    /// </summary>
    public required string FirstName { get; set; }

    /// <summary>
    ///
    /// </summary>
    public required string LastName { get; set; }

    /// <summary>
    ///
    /// </summary>
    public required string Username { get; set; }

    /// <summary>
    ///
    /// </summary>
    public required string Password { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public bool IsActive { get; set; }
}