using Shared.Core.BaseEntities;

namespace Driver.Domain.Entities;

public class DriverCarEntity : BaseAuditableEntity
{
    public Guid DriverId { get; set; }
    public DriverEntity Driver { get; set; }

    public Guid CarId { get; set; }
    public CarEntity Car { get; set; }
}