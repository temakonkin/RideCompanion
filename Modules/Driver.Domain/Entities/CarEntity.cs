using Shared.Core.BaseEntities;

namespace Driver.Domain.Entities;

public class CarEntity : BaseAuditableEntity
{
    public Guid VehicleDictionaryId { get; set; }
    public VehicleDictionaryEntity VehicleDictionary { get; set; }

    public string Number { get; set; }
    public string Color { get; set; }
    public short PassengersCount { get; set; }
}