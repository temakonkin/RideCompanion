using Shared.Core.BaseEntities;

namespace Driver.Domain.Entities;

public class VehicleDictionaryEntity : BaseAuditableEntity
{
    public string Brand { get; set; }
    public string Model { get; set; }
    public short Year { get; set; }
}