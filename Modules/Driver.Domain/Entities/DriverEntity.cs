using Shared.Core.BaseEntities;

namespace Driver.Domain.Entities;

public class DriverEntity : BaseAuditableEntity
{
    public Guid UserId { get; set; }
}