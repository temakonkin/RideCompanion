using Identity.Domain.Dto;
using Identity.Domain.Entities;
using Identity.Infrastructure.Context;
using MediatR;

namespace Identity.App.Mediatr.Commands;

/// <summary>
///
/// </summary>
/// <param name="UserDto"></param>
public record CreateUserCommand(UserDto UserDto) : IRequest<UserDto>;

public class CreateUserCommandHandler(IIdentityDbContext context) : IRequestHandler<CreateUserCommand, UserDto>
{
    public async Task<UserDto> Handle(CreateUserCommand request, CancellationToken cancellationToken)
    {
        context.Users.Add(new UserEntity
        {
            Id = Guid.NewGuid(),
            Created = default,
            CreatedBy = default,
            LastModified = null,
            LastModifiedBy = null,
            FirstName = request.UserDto.FirstName,
            LastName = request.UserDto.LastName,
            Username = request.UserDto.Username,
            Password = request.UserDto.Password
        });
        var result = await context.SaveChangesAsync(cancellationToken);
        return result > 0
            ? request.UserDto
            : throw new Exception("Failed to create user");
    }
}