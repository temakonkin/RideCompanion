using Dapper;
using Identity.Domain.Dto;
using Identity.Infrastructure.Context;
using MediatR;
using Shared.Core.Interfaces;

namespace Identity.App.Mediatr.Queries;

/// <summary>
/// Get user by id
/// </summary>
/// <param name="id"> User id </param>
public record GetUserByIdQuery(Guid id) : IRequest<UserDto>;

public class GetUserByIdQueryHandler(IIdentityDbContext context, IDapperContext dapper) : IRequestHandler<GetUserByIdQuery, UserDto>
{
    public async Task<UserDto> Handle(GetUserByIdQuery request, CancellationToken cancellationToken)
    {
        if (request.id == Guid.Empty)
            return null;

        var connection = dapper.ConnectToIdentityDb();
        connection.Open();
        connection.BeginTransaction();
        var userData = await connection.QueryFirstOrDefaultAsync<UserDto>("""
            SELECT "Id", "FirstName", "LastName", "Username", "Password"
            FROM
                "Users"
            WHERE
                "Id" = @id
        """);
        connection.Close();

        return userData;
    }
}