﻿FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS base
USER $APP_UID
WORKDIR /app
EXPOSE 8080
EXPOSE 8081

FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
ARG BUILD_CONFIGURATION=Release
WORKDIR /src
COPY ["RideCompanion.Server/RideCompanion.Server.csproj", "RideCompanion.Server/"]
COPY ["Modules/Identity.App/Identity.App.csproj", "Modules/Identity.App/"]
COPY ["Modules/Identity.Infrastructure/Identity.Infrastructure.csproj", "Modules/Identity.Infrastructure/"]
COPY ["Modules/Identity.Domain/Identity.Domain.csproj", "Modules/Identity.Domain/"]
COPY ["Shared/Shared.Core/Shared.Core.csproj", "Shared/Shared.Core/"]
COPY ["Modules/Admin.App/Admin.App.csproj", "Modules/Admin.App/"]
COPY ["Modules/Admin.Infrastructure/Admin.Infrastructure.csproj", "Modules/Admin.Infrastructure/"]
COPY ["Modules/Admin.Domain/Admin.Domain.csproj", "Modules/Admin.Domain/"]
COPY ["Modules/Companion.App/Companion.App.csproj", "Modules/Companion.App/"]
COPY ["Modules/Companion.Infrastructure/Companion.Infrastructure.csproj", "Modules/Companion.Infrastructure/"]
COPY ["Modules/Companion.Domain/Companion.Domain.csproj", "Modules/Companion.Domain/"]
COPY ["Modules/Driver.App/Driver.App.csproj", "Modules/Driver.App/"]
COPY ["Modules/Driver.Infrastructure/Driver.Infrastructure.csproj", "Modules/Driver.Infrastructure/"]
COPY ["Modules/Driver.Domain/Driver.Domain.csproj", "Modules/Driver.Domain/"]
COPY ["Modules/Ride.App/Ride.App.csproj", "Modules/Ride.App/"]
COPY ["Modules/Ride.Infrastructure/Ride.Infrastructure.csproj", "Modules/Ride.Infrastructure/"]
COPY ["Modules/Ride.Domain/Ride.Domain.csproj", "Modules/Ride.Domain/"]
COPY ["Shared/Shared.Migrations/Shared.Migrations.csproj", "Shared/Shared.Migrations/"]
COPY ["ridecompanion.client/ridecompanion.client.esproj", "ridecompanion.client/"]
RUN dotnet restore "RideCompanion.Server/RideCompanion.Server.csproj"
COPY . .
WORKDIR "/src/RideCompanion.Server"
RUN dotnet build "RideCompanion.Server.csproj" -c $BUILD_CONFIGURATION -o /app/build

FROM build AS publish
ARG BUILD_CONFIGURATION=Release
RUN dotnet publish "RideCompanion.Server.csproj" -c $BUILD_CONFIGURATION -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "RideCompanion.Server.dll"]
