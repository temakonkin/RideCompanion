using RideCompanion.Server.Middleware;

namespace RideCompanion.Server.Config;

/// <summary>
///
/// </summary>
public static class AppConfig
{
    /// <summary>
    ///
    /// </summary>
    /// <param name="webApplication"></param>
    public static void Configure(this WebApplication webApplication)
    {
        webApplication.UseCors("AllowAllOrigins");
        webApplication.UseDefaultFiles();
        webApplication.UseStaticFiles();
        webApplication.UseMiddleware<JwtMiddleware>();
        webApplication.UseHsts();
        webApplication.UseSwagger();
        webApplication.UseSwaggerUI();
        webApplication.UseHttpsRedirection();
        webApplication.UseAuthorization();
        webApplication.UseStaticFiles();
        webApplication.UseRouting();
        webApplication.MapControllerRoute(name: "default", pattern: "{controller}/{action=Index}/{id?}");
        webApplication.MapFallbackToFile("index.html");
    }
}