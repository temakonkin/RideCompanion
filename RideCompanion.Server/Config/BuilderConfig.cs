using Identity.Infrastructure;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using RideCompanion.Server.Filters.Exceptions;
using Shared.Core;

namespace RideCompanion.Server.Config;

/// <summary>
///
/// </summary>
public static class BuilderConfig
{
    /// <summary>
    ///
    /// </summary>
    /// <param name="builder"></param>
    public static void AddCors(this WebApplicationBuilder builder)
    {
        builder.Services.AddCors(options =>
        {
            options.AddPolicy("AllowSpecificOrigin", policy =>
            {
                policy
                    .WithOrigins("https://ridecompanion.net")
                    .AllowAnyHeader()
                    .AllowAnyMethod();
            });

            options.AddPolicy("AllowAllOrigins", policy =>
            {
                policy
                    .AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod();
            });
        });
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="builder"></param>
    public static void AddAuthenticate(this WebApplicationBuilder builder)
    {
        builder.Services.AddAuthorization();

        builder
            .Services
            .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true, // указывает, будет ли проверяться издатель при валидации токена
                    ValidIssuer = AuthOptions.ISSUER, // строка, представляющая издателя
                    ValidateAudience = true, // будет ли проверяться потребитель токена
                    ValidAudience = AuthOptions.AUDIENCE, // установка потребителя токена
                    ValidateLifetime = true, // будет ли проверяться время существования
                    IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(), // установка ключа безопасности
                    ValidateIssuerSigningKey = true, // валидация ключа безопасности
                };
            });
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="builder"></param>
    public static void AddSwagger(this WebApplicationBuilder builder)
    {
        builder.Services.AddSwaggerGen(swagger =>
        {
            swagger.SwaggerDoc("v1", new OpenApiInfo
            {
                Version = "v1",
                Title = "JWT Token Authentication API",
                Description = ".NET 8 Web API"
            });

            swagger.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
            {
                Name = "Authorization",
                Type = SecuritySchemeType.ApiKey,
                Scheme = "Bearer",
                BearerFormat = "JWT",
                In = ParameterLocation.Header,
                Description = "JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
            });

            swagger.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        }
                    },
                    Array.Empty<string>()
                }
            });
        });
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="builder"></param>
    public static void AddServices(this WebApplicationBuilder builder)
    {
        builder.Services.InjectCore();
        builder.Services.Configure<AppSettings>(builder.Configuration.GetSection("AppSettings"));
        builder.Services.InjectIdentity(builder.Configuration);

        builder.Services.AddControllers(options =>
        {
            options.Filters.Add<TimeoutExceptionFilter>();
        });

        builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(typeof(Program).Assembly));
    }
}