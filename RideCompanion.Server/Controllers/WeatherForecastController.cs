using Identity.App.Helpers;
using Microsoft.AspNetCore.Mvc;
using RideCompanion.Server.Controllers.Base;
using RideCompanion.Server.Filters.Exceptions;

namespace RideCompanion.Server.Controllers;

[Authorize]
[ApiController]
[Route("[controller]")]
public class WeatherForecastController(
    ILogger<WeatherForecastController> logger
) : BaseController
{
    private static readonly string[] Summaries =
    [
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching",
    ];

    [HttpGet(Name = "GetWeatherForecast")]
    public async Task<IEnumerable<WeatherForecast>> Get()
    {
        var result = await GetData();

        logger.LogInformation("Getting Weather Forecast");
        await Console.Error.WriteLineAsync("Getting Weather Forecast");

        return result;
    }

    private async Task<WeatherForecast[]?> GetData()
    {
        await Task.Delay(1000);

        var data = Enumerable
            .Range(1, 5)
            .Select(index => new WeatherForecast
            {
                Date = DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
            .ToArray();

        throw new TimeoutException("Timeout Exception");

        return data;
    }
}