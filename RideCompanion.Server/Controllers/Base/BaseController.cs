using Identity.Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace RideCompanion.Server.Controllers.Base;

public abstract class BaseController : ControllerBase
{
    private IUserService? _userService;

    protected IUserService UserService
    {
        get
        {
            if (_userService == null)
            {
                _userService = HttpContext?.RequestServices.GetService<IUserService>()
                               ?? throw new InvalidOperationException("HttpContext is null or IUserService not registered.");
            }
            return _userService;
        }
    }
}