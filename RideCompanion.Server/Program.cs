using RideCompanion.Server.Config;
AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

var builder = WebApplication.CreateBuilder(args);
builder.AddCors();
builder.AddAuthenticate();
builder.AddSwagger();
builder.AddServices();

var app = builder.Build();
app.Configure();
app.Run();