﻿using Identity.Infrastructure.Services.Interfaces;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Shared.Core;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace RideCompanion.Server.Middleware;

/// <summary>
///
/// </summary>
/// <param name="next"></param>
/// <param name="appSettings"></param>
public class JwtMiddleware( RequestDelegate next, IOptions<AppSettings> appSettings )
{
    /// <summary>
    ///
    /// </summary>
    /// <param name="context"></param>
    /// <param name="userService"></param>
    public async Task Invoke(HttpContext context, IUserService userService)
    {
        var token = context.Request.Headers.Authorization.FirstOrDefault()?.Split(" ").Last();

        #if DEBUG
            Console.WriteLine("Get token: {0}, {1}", token, context.Request.GetDisplayUrl());
        #endif

        if (token != null)
            await AttachUserToContext(context, userService, token);

        await next(context);
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="context"></param>
    /// <param name="userService"></param>
    /// <param name="token"></param>
    private async Task AttachUserToContext(HttpContext context, IUserService userService, string token)
    {
        try
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(appSettings.Value.Secret);

            #if DEBUG
                Console.WriteLine($"Check token: {token}");
            #endif

            tokenHandler.ValidateToken(token, new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                ClockSkew = TimeSpan.Zero
            }, out var validatedToken);

            var jwtToken = (JwtSecurityToken)validatedToken;
            var userId = Guid.Parse(jwtToken.Claims.First(x => x.Type == "id").Value);

            context.Items["UserEntity"] = await userService.GetById(userId);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Token validation error: {ex.Message}");
        }
    }
}