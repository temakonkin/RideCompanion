﻿using Identity.App.Helpers;
using Identity.Domain.Dto;
using Identity.Infrastructure.Services;
using Microsoft.AspNetCore.Mvc;
using RideCompanion.Server.Controllers.Base;

namespace RideCompanion.Server.Areas.Identity.Controllers;

/// <summary>
/// Identity controller
/// </summary>
[ApiController]
[Route("api/[controller]")]
public class IdentityController : BaseController
{
    /// <summary>
    ///
    /// </summary>
    /// <param name="model"></param>
    /// <returns></returns>
    [HttpGet("authenticate")]
    public async Task<IActionResult> Authenticate([FromQuery] AuthenticateRequest model)
    {
        var response = await UserService.Authenticate(model);
        if (response == null)
            return BadRequest(new { message = "Username or password is incorrect" });

        return Ok(response);
    }

    /// <summary>
    /// Creates user
    /// </summary>
    /// <param name="userObj"> User object </param>
    /// <returns> User dto </returns>
    [Authorize]
    [HttpPost("createUser")]
    public async Task<IActionResult> CreateUser([FromBody] UserDto userObj)
    {
        userObj.Id = Guid.Empty;
        return Ok(await UserService.AddAndUpdateUser(userObj));
    }

    /// <summary>
    /// Updates user
    /// </summary>
    /// <param name="id"> User id </param>
    /// <param name="userObj"> User object </param>
    /// <returns> User dto </returns>
    [Authorize]
    [HttpPut("{id:guid}")]
    public async Task<IActionResult> UpdateUser(Guid id, [FromBody] UserDto userObj)
    {
        return Ok(await UserService.AddAndUpdateUser(userObj));
    }
}