using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Shared.Core.Dtos;

namespace RideCompanion.Server.Filters.Exceptions;

/// <summary>
/// Filter for timeout exception
/// </summary>
public class TimeoutExceptionFilter : IExceptionFilter
{
    public void OnException(ExceptionContext context)
    {
        Console.Error.WriteLine(context.Exception.Message);

        if (context.Exception is not TimeoutException)
            return;

        context.Result = new JsonResult(new Response
        {
            Message = "Service timeout",
            StatusCode = 408
        })
        {
            StatusCode = 408
        };

        context.ExceptionHandled = true;
    }
}