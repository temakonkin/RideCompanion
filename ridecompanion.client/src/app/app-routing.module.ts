import { NgModule, OnInit } from '@angular/core';
import { initDrawers, initFlowbite } from 'flowbite';
import { RouterModule, Routes } from '@angular/router';
import { TelegramService } from './shared/services/telegram.service';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [TelegramService],
})
export class AppRoutingModule implements OnInit {
  ngOnInit(): void {
    window.addEventListener('load', initDrawers);
    initFlowbite();
  }
}
