import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TitleService } from './shared/services/title.service';
import { AuthService } from './shared/services/auth/auth.service';

@NgModule({
  declarations: [
  ],
  imports: [
    BrowserModule, AppRoutingModule, AppComponent
  ],
  providers: [TitleService, AuthService],
  bootstrap: []
})
export class AppModule { }
