import { Component, inject } from '@angular/core';
import { TelegramService } from '../../shared/services/telegram.service';
import { initDrawers, initFlowbite } from 'flowbite';
import { TitleService } from '../../shared/services/title.service';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [],
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss',
})
export class HomeComponent {
  telegram = inject(TelegramService);

  constructor(private titleService: TitleService) {
    //this.telegram.BackButton.hide();
    this.titleService.changeTitle('Home');
  }

  ngOnInit(): void {
    window.addEventListener('load', initDrawers);
    initFlowbite();
  }
}
