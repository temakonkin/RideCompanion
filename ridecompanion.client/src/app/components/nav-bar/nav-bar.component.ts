import { Component, inject, OnInit } from '@angular/core';
import { NgIconComponent, provideIcons } from '@ng-icons/core';
import { RouterLink, RouterOutlet, RouterLinkActive } from '@angular/router';
import { CommonModule } from '@angular/common';
import {
  bootstrapHouseFill,
  bootstrapPersonFill,
  bootstrapCarFrontFill,
  bootstrapGeoAltFill,
  bootstrapMapFill,
  bootstrapInfoCircleFill,
} from '@ng-icons/bootstrap-icons';
import { TelegramService, WebAppUser } from '../../shared/services/telegram.service';

@Component({
  selector: 'app-nav-bar',
  standalone: true,
  imports: [
    NgIconComponent,
    CommonModule,
    RouterOutlet,
    RouterLink,
    RouterLinkActive,
  ],
  templateUrl: './nav-bar.component.html',
  styleUrl: './nav-bar.component.scss',
  viewProviders: [
    provideIcons({
      bootstrapHouseFill,
      bootstrapPersonFill,
      bootstrapCarFrontFill,
      bootstrapGeoAltFill,
      bootstrapMapFill,
      bootstrapInfoCircleFill,
    }),
  ],
})
export class NavBarComponent implements OnInit {
  telegram = inject(TelegramService);
  public user: WebAppUser | undefined;

  constructor() {
    this.telegram.BackButton.hide();
    this.telegram.sendData({ command: 'get_user' });
  }

  ngOnInit(): void {
    this.user = this.telegram.getUser();
    console.log(this.user);
  }
}
