import { Component, Input, TemplateRef, inject } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
    selector: 'app-offcanvas',
    standalone: true,
    imports: [CommonModule],
    templateUrl: './offcanvas.component.html',
    styleUrls: ['./offcanvas.component.scss'],
})
export class OffcanvasComponent {
    @Input() content!: TemplateRef<any>;
}
