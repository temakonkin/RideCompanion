import { Component } from '@angular/core';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { TitleService } from '../../shared/services/title.service';

@Component({
  selector: 'app-not-found',
  standalone: true,
  imports: [RouterLink, RouterLinkActive],
  templateUrl: './not-found.component.html',
  styleUrl: './not-found.component.scss'
})
export class NotFoundComponent {
  constructor(private titleService: TitleService) {
    this.titleService.changeTitle('Not found');
  }
}
