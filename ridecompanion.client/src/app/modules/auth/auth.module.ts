import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { LogInComponent } from './components/log-in/log-in.component';
import { AuthService } from '../../shared/services/auth/auth.service';
import { AuthComponent } from './auth.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    LogInComponent,
    SignInComponent
  ],
  exports: [],
  providers: [
    AuthService
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild([
      { path: '', component: AuthComponent },
      { path: 'login', component: LogInComponent },
      { path: 'signin', component: SignInComponent },
    ]),
  ],
})
export class AuthModule {
  OnInit() {
  }
}
