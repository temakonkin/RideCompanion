import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../../shared/services/auth/auth.service';
import { FormControl, FormGroup } from '@angular/forms';
import { IAuthData } from '../../../../shared/models/auth/IAuthData';
import { StorageService } from '../../../../shared/services/storage/storage.service';

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrl: './log-in.component.css',
})
export class LogInComponent implements OnInit {
  constructor(private authService: AuthService, private storageService: StorageService) {}

  loginForm = new FormGroup({
    login: new FormControl(''),
    password: new FormControl(''),
  });

  authData: IAuthData = {
    id: '',
    firstName: '',
    lastName: '',
    username: '',
    token: '',
  };

  ngOnInit(): void {
  }

  onSubmit() {
    console.warn(this.loginForm.value);
  }

  login(): void {
    this.authService
      .login(this.loginForm.value.login!, this.loginForm.value.password!)
      .subscribe({
        next: (response) => {
          this.authData = response;
        },
        error: (err) => {
          console.error('Login error:', err);
        },
        complete: () => {
          console.log('Login request completed');
        },
      });

    console.error('Result: ' + this.authData.token);

    this.storageService.saveUser(this.authData);
    var user = this.storageService.getUser();
    console.log('User token from storage: ' + user?.token);

    if (this.authData.token) {
      window.location.href = '/';
    }
  }
}
