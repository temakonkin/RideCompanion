import { Component, OnInit } from '@angular/core';
import { StorageService } from '../../shared/services/storage/storage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-simple-component',
  template: `<p>Auth component</p>`,
  styles: []
})
export class AuthComponent implements OnInit {
  currentUser: any;

  constructor(private storageService: StorageService, private router: Router) {}

  ngOnInit(): void {
    this.currentUser = this.storageService.getUser();
    if(this.currentUser === null) {
      console.log("Current user: " +this.currentUser);
      this.router.navigate(['/auth/login']);
    }
  }
}
