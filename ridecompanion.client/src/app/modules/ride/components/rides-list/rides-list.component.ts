import { Component, OnInit } from '@angular/core';
import { RideRouteComponent } from '../ride-route/ride-route.component';
import { RideListService } from '../../../../shared/services/ride/ride-list.service';
import { TitleService } from '../../../../shared/services/title.service';

@Component({
  selector: 'app-rides-list',
  standalone: true,
  imports: [RideRouteComponent],
  templateUrl: './rides-list.component.html',
  styleUrl: './rides-list.component.scss',
})
export class RidesListComponent implements OnInit {
  constructor(
    private rideListService: RideListService,
    private titleService: TitleService
  ) {
    this.titleService.changeTitle('Rides');
  }
  ngOnInit(): void {
    var dataFromServe = this.rideListService.getWeatherForecast().subscribe({
      next: (response) => {
        console.log(response);
      },
      error: (err) => {
        console.error('Weather forecast error:', err);
      },
      complete: () => {
        console.log('Weather forecast request completed');
      },
    });
  }

  protected ridesList = this.rideListService.getRides();
}
