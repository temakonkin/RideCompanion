import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {WelcomeComponent} from "./components/welcome/welcome.component";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    WelcomeComponent
  ]
})
export class HomeModule { }
