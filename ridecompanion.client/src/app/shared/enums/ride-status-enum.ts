export enum RideStatusEnum {
  Pending = 1,
  Accepted = 2,
  Rejected = 3,
  Canceled = 4,
  Finished = 5
}
