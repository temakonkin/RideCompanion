import { StorageService } from './../storage/storage.service';
import { Injectable } from '@angular/core';
import { ICompanion } from '../../models/copanion/ICompanion';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const API_URL = 'https://localhost:44320/';

@Injectable({
  providedIn: 'root',
})
export class CompanionService {
  private token: string | undefined;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    var user = this.storageService.getUser();
    this.token = user?.token;
  }

  private companionsList: ICompanion[] = [
    {
      id: '1',
      name: 'Leanne Graham',
      username: 'Bret',
      email: 'Sincere@april.biz',
      birthDate: new Date('01.01.1990'),
      address: {
        street: 'Kulas Light',
        suite: 'Apt. 556',
        city: 'Gwenborough',
        zipcode: '92998-3874',
        geo: {
          lat: '-37.3159',
          lng: '81.1496',
        },
      },
      phone: '1-770-736-8031 x56442',
      website: 'hildegard.org',
    },
    {
      id: '2',
      name: 'Ervin Howell',
      username: 'Antonette',
      email: 'Shanna@melissa.tv',
      birthDate: new Date('01.01.1990'),
      address: {
        street: 'Victor Plains',
        suite: 'Suite 879',
        city: 'Wisokyburgh',
        zipcode: '90566-7771',
        geo: {
          lat: '-43.9509',
          lng: '-34.4618',
        },
      },
      phone: '010-692-6593 x09125',
      website: 'anastasia.net',
    },
  ];

  getCompanions(): ICompanion[] {
    return this.companionsList;
  }

  getWeatherForecast(): Observable<any> {
    const headers = new HttpHeaders({
      accept: 'text/plain',
      Authorization: `Bearer ${this.token}`,
    });

    return this.http.get(API_URL + 'weatherforecast', { headers });
  }
}
