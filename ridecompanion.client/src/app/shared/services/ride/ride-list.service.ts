import { Injectable } from '@angular/core';
import { IRide } from '../../models/ride/IRide';
import { RideStatusEnum } from '../../enums/ride-status-enum';
import { CompanionService } from '../companion/companion.service';
import { DriverService } from '../driver/driver.service';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StorageService } from '../storage/storage.service';

const API_URL = 'https://localhost:44320/';

@Injectable({
  providedIn: 'root',
})
export class RideListService {
  private token: string | undefined;

  constructor(
    private http: HttpClient,
    private storageService: StorageService,
    private companionService: CompanionService,
    private driverService: DriverService
  ) {
    var user = this.storageService.getUser();
    this.token = user?.token;
  }

  private rides: IRide[] = [
    {
      id: '1',
      from: 'Bucharest',
      to: 'Brasov',
      date: new Date('2019-02-01'),
      price: 100,
      status: RideStatusEnum.Pending,
      companionId: '',
      companion: this.companionService.getCompanions()[0],
      driverId: '',
      driver: this.driverService.getDrivers()[0],
    },
  ];

  getRides(): IRide[] {
    return this.rides;
  }

  getWeatherForecast(): Observable<any> {
    const headers = new HttpHeaders({
      accept: 'text/plain',
      Authorization: `Bearer ${this.token}`,
    });

    return this.http.get(API_URL + 'weatherforecast', { headers });
  }
}
