import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';

interface TgButton {
  show(): void;
  hide(): void;
  setText(text: string): void;
  onClick(fn: Function): void;
  offClick(fn: Function): void;
  enable(): void;
  disable(): void;
}

export interface WebAppUser {
  id: number;
  is_bot?: boolean;
  first_name: string;
  last_name?: string;
  username?: string;
  language_code?: string;
  is_premium?: boolean;
  added_to_attachment_menu?: boolean;
  allows_write_to_pm?: boolean;
  photo_url?: string;
}

declare global {
  interface Window {
    Telegram: any;
  }
}

@Injectable({
  providedIn: 'root',
})
export class TelegramService {
  private window;
  tg;
  constructor(@Inject(DOCUMENT) private document: Document) {
    this.window = this.document.defaultView;
    this.tg = this.window?.Telegram.WebApp;
    this.tg.setHeaderColor('#FFFFFF');
  }

  get MainButton(): TgButton {
    return this.tg.MainButton;
  }

  get BackButton(): TgButton {
    return this.tg.BackButton;
  }

  getUser(): WebAppUser {
    return this.tg.initDataUnsafe?.user;
  }

  sendData(data: object) {
    this.tg.sendData(JSON.stringify(data));
  }

  ready() {
    this.tg.ready();
  }
}
