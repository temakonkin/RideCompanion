export interface IAuthData {
  id: string;
  firstName: string;
  lastName: string;
  username: string;
  token: string;
}
