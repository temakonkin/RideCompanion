import { TitleService } from './shared/services/title.service';
import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { initFlowbite } from 'flowbite';
import 'flowbite';
import { initDrawers } from 'flowbite';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, NavBarComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent implements OnInit {
  title: string = 'RideCompanion';

  constructor(private titleService: TitleService) {}

  ngOnInit(): void {
    this.titleService.currentTitle.subscribe(title => this.title = title);
    window.addEventListener('load', initDrawers);
    initFlowbite();
  }
}
