﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Shared.Migrations.Migrations
{
    /// <inheritdoc />
    public partial class UserDevices : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    FirstName = table.Column<string>(type: "text", nullable: false),
                    LastName = table.Column<string>(type: "text", nullable: false),
                    Username = table.Column<string>(type: "text", nullable: false),
                    Password = table.Column<string>(type: "text", nullable: false),
                    IsActive = table.Column<bool>(type: "boolean", nullable: false),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    CreatedBy = table.Column<Guid>(type: "uuid", nullable: false),
                    LastModified = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    LastModifiedBy = table.Column<Guid>(type: "uuid", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Created", "CreatedBy", "FirstName", "IsActive", "LastModified", "LastModifiedBy", "LastName", "Password", "Username" },
                values: new object[,]
                {
                    { new Guid("03aec35d-dc10-4412-9e6d-68cef0cad79b"), new DateTime(2024, 6, 7, 15, 45, 36, 946, DateTimeKind.Local).AddTicks(8741), new Guid("00000000-0000-0000-0000-000000000000"), "Manager", true, null, null, "System", "manager", "manager" },
                    { new Guid("39c336ef-b727-4bd7-8b18-ac8b83ab7f19"), new DateTime(2024, 6, 7, 15, 45, 36, 946, DateTimeKind.Local).AddTicks(8696), new Guid("00000000-0000-0000-0000-000000000000"), "Admin", true, null, null, "System", "admin", "admin" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
