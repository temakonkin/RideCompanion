dotnet ef migrations add UserDevices --project .\Shared\Shared.Migrations\Shared.Migrations.csproj --startup-project .\RideCompanion.Server\RideCompanion.Server.csproj --context IdentityDbContext

dotnet ef database update --context IdentityDbContext --project .\Shared\Shared.Migrations\ --startup-project .\RideCompanion.Server\