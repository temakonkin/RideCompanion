using Shared.SoftResult.Interfaces;

namespace Shared.SoftResult.ResultTypes;

sealed class Error : AbstractError
{
    public Error(string message)
    {
        Message = message;
    }

    public Error(string key, object value)
    {
        Metadata = new Dictionary<string, object>
        {
            {
                key, value
            },
        };
    }

    public Error(string message, string key, object value)
    {
        Message = message;

        Metadata = new Dictionary<string, object>
        {
            {
                key, value
            },
        };
    }

    public Error(Dictionary<string, object> metadata)
    {
        Metadata = metadata;
    }

    public Error(string message, Dictionary<string, object> metadata)
    {
        Message = message;
        Metadata = metadata;
    }

    public override string GetMessage()
    {
        return Message ?? string.Empty;
    }

    public override Dictionary<string, object> GetMetadata()
    {
        return Metadata ?? new Dictionary<string, object>();
    }
}