using Microsoft.AspNetCore.Mvc;
using Shared.SoftResult.Interfaces;

namespace Shared.SoftResult.ResultTypes;

public class Result : ISoftResult
{
    public string Message
    {
        get;
    }

    public static ISoftResult Ok(string message)
    {
        throw new NotImplementedException();
    }

    public static ISoftResult BadRequest(string message)
    {
        throw new NotImplementedException();
    }

    public static ISoftResult BadRequest(string message, IError error)
    {
        throw new NotImplementedException();
    }

    public static ISoftResult BadRequest(string message, List<IError> error)
    {
        throw new NotImplementedException();
    }

    public static ISoftResult NotFound(string message)
    {
        throw new NotImplementedException();
    }

    public static ISoftResult NotFound(string message, IError error)
    {
        throw new NotImplementedException();
    }

    public static ISoftResult NotFound(string message, List<IError> error)
    {
        throw new NotImplementedException();
    }

    public Task ExecuteResultAsync(ActionContext context)
    {
        throw new NotImplementedException();
    }
}

sealed class Result<T> : Result, ISoftResult<T>
{
    public string Message
    {
        get;
    }

    public static ISoftResult Ok(T data)
    {
        throw new NotImplementedException();
    }

    public static ISoftResult Ok(T data, string message)
    {
        throw new NotImplementedException();
    }
}