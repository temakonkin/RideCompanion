using Microsoft.AspNetCore.Mvc;

namespace Shared.SoftResult.Interfaces;

public interface ISoftResult : IActionResult
{
    string Message { get; }

    abstract static ISoftResult Ok(string message);

    abstract static ISoftResult BadRequest(string message);
    abstract static ISoftResult BadRequest(string message, IError error);
    abstract static ISoftResult BadRequest(string message, List<IError> error);

    abstract static ISoftResult NotFound(string message);
    abstract static ISoftResult NotFound(string message, IError error);
    abstract static ISoftResult NotFound(string message, List<IError> error);
}

internal interface ISoftResult<in T> : ISoftResult
{
    abstract static ISoftResult Ok(T data);
    abstract static ISoftResult Ok(T data, string message);
}