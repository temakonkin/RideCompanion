namespace Shared.SoftResult.Interfaces;

internal abstract class AbstractError : IError
{
    protected string? Message { get; set; }
    protected Dictionary<string, object>? Metadata { get; set; }

    public abstract string GetMessage();
    public abstract Dictionary<string, object> GetMetadata();
}