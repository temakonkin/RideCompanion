using System.Data;

namespace Shared.Core.Interfaces;

public interface IDapperContext
{
    IDbConnection ConnectToIdentityDb();
}