namespace Shared.Core.Interfaces;

public interface IDapperConnections
{
    public string IdentityDbConnection { get; }
}