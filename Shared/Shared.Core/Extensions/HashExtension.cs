using System.Security.Cryptography;
using System.Text;

namespace Shared.Core.Extensions;

public static class HashExtension
{
    public static string Hash(this string data)
    {
        var hashedBytes = SHA256.HashData(Encoding.UTF8.GetBytes(data));
        return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
    }
}