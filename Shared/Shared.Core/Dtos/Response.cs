namespace Shared.Core.Dtos;

public class Response
{
    public string Message { get; set; }
    public int StatusCode { get; set; }
}