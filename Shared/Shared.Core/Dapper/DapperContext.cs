using Microsoft.Extensions.Configuration;
using Npgsql;
using Shared.Core.Interfaces;
using System.Data;

namespace Shared.Core.Dapper;

/// <summary>
/// Dapper factory
/// </summary>
public class DapperContext : IDapperContext
{
    private readonly IConfiguration _configuration;
    private IDapperConnections Connections
    {
        get
        {
            return new DapperConnections(_configuration);
        }
    }

    /// <summary>
    /// Private dapper constructor
    /// </summary>
    /// <param name="configuration"> Configuration </param>
    public DapperContext(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    /// <summary>
    /// Connect to IdentityDb
    /// </summary>
    /// <returns></returns>
    public IDbConnection ConnectToIdentityDb()
        => GetConnection(Connections.IdentityDbConnection);

    /// <summary>
    /// Get connection
    /// </summary>
    /// <param name="connectionString"> Connection string </param>
    /// <returns></returns>
    private IDbConnection GetConnection(string connectionString)
        => new NpgsqlConnection(_configuration.GetConnectionString(connectionString));
}