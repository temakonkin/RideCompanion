using Microsoft.Extensions.Configuration;
using Shared.Core.Interfaces;

namespace Shared.Core.Dapper;

public class DapperConnections(
    IConfiguration configuration
) : IDapperConnections
{
    public string IdentityDbConnection
    {
        get
        {
            return configuration.GetConnectionString("IdentityDbConnection")!;
        }
    }
}