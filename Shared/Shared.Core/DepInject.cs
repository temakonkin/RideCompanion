using Microsoft.Extensions.DependencyInjection;
using Shared.Core.Dapper;
using Shared.Core.Interfaces;

namespace Shared.Core;

/// <summary>
///
/// </summary>
public static class DepInject
{
    /// <summary>
    ///
    /// </summary>
    /// <param name="services"></param>
    public static void InjectCore(this IServiceCollection services)
    {
        services.AddTransient<IDapperContext, DapperContext>();
    }
}