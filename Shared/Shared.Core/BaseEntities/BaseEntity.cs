﻿namespace Shared.Core.BaseEntities;

public abstract class BaseEntity
{
    public Guid Id { get; set; }
}